import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

public class CalculatorTests {

    public AndroidDriver<MobileElement> driver;
    public WebDriverWait wait;

    // Elements

    @BeforeMethod
    public void setup() throws MalformedURLException {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("deviceName","Nexus_S_API_3_mydevice");
        caps.setCapability("platformName","Android");
        caps.setCapability("udid","emulator-5556");
        caps.setCapability("platformVersion","11");
        caps.setCapability("appPackage","com.google.android.calculator");
        caps.setCapability("appActivity","com.android.calculator2.Calculator");
        caps.setCapability("noReset","false");
        driver= new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), caps);
        wait = new WebDriverWait(driver,10);
    }

    @Test
    public void basicTest() throws InterruptedException {
        System.out.println("Hello Automation using Appium!!! ROCKS!");
    }

    @AfterMethod
    public void tearDown(){
        driver.quit();
    }

}
